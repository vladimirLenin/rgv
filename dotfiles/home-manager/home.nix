{lib, config, pkgs, sources, ... }:



let

#  nurNoPkgs = import <nur> { pkgs = null; };

 nurNoPkgs =  import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {}; # <nur> { pkgs = null; };
  #nurNoPkgs = import (import /etc/nix/sources.nix).nur { };
  
/*  doom-emacs = pkgs.callPackage (builtins.fetchTarball {
    url = https://github.com/vlaci/nix-doom-emacs/archive/master.tar.gz;
  }) {
    doomPrivateDir = ./doom.d;  # Directory containing your config.el init.el
                                # and packages.el files
    emacsPackagesOverlay = self: super: {
     magit-delta = super.magit-delta.overrideAttrs (esuper: {
       buildInputs = esuper.buildInputs ++ [ pkgs.git ];
     });
  };
  };
  */
in
{

  imports= [ nurNoPkgs.repos.rycee.hmModules.emacs-init];
 /*
 nixpkgs.overlays = [
    (import sources.emacs-overlay)
    (self: super: {
      emacs27 = super.emacs27.overrideAttrs (old: {
        patches = old.patches or [] ++ [
          # Fix "Attempt to shape unibyte text" error.
          (pkgs.fetchpatch {
            url = "https://github.com/emacs-mirror/emacs/commit/fe903c5ab7354b97f80ecf1b01ca3ff1027be446.patch";
            sha256 = "0zldjs8nx26x7r8pwjc995lvpg06iv52rq4cy1w38hxhy7vp8lp3";
          })
        ];
      });
    })
  ];
 */
 /* programs.emacs.init = {
  
                             enable = true;
                             recommendedGcSettings = true;
   
    earlyInit = ''
      ;; Disable some GUI distractions. We set these manually to avoid starting
      ;; the corresponding minor modes.
      (push '(menu-bar-lines . 0) default-frame-alist)
      (push '(tool-bar-lines . nil) default-frame-alist)
      (push '(vertical-scroll-bars . nil) default-frame-alist)

      ;; Set up fonts early.
      (set-face-attribute 'default
                          nil
                          :height 80
                          :family "Fantasque Sans Mono")
      (set-face-attribute 'variable-pitch
                          nil
                          :family "DejaVu Sans")
    '';


    prelude = ''
      ;; Disable startup message.
      (setq inhibit-startup-screen t
            inhibit-startup-echo-area-message (user-login-name))

      (setq initial-major-mode 'fundamental-mode
            initial-scratch-message nil)

      ;; Don't blink the cursor.
      (setq blink-cursor-mode nil)

      ;; Set frame title.
      (setq frame-title-format
            '("" invocation-name ": "(:eval
                                      (if (buffer-file-name)
                                          (abbreviate-file-name (buffer-file-name))
                                        "%b"))))

      ;; Accept 'y' and 'n' rather than 'yes' and 'no'.
      (defalias 'yes-or-no-p 'y-or-n-p)

      ;; Don't want to move based on visual line.
      (setq line-move-visual nil)

      ;; Stop creating backup and autosave files.
      (setq make-backup-files nil
            auto-save-default nil)

      ;; Default is 4k, which is too low for LSP.
      (setq read-process-output-max (* 1024 1024))

      ;; Always show line and column number in the mode line.
      (line-number-mode)
      (column-number-mode)

      ;; Enable some features that are disabled by default.
      (put 'narrow-to-region 'disabled nil)

      ;; Typically, I only want spaces when pressing the TAB key. I also
      ;; want 4 of them.
      (setq-default indent-tabs-mode nil
                    tab-width 4
                    c-basic-offset 4)

      ;; Trailing white space are banned!
      (setq-default show-trailing-whitespace t)

      ;; Make a reasonable attempt at using one space sentence separation.
      (setq sentence-end "[.?!][]\"')}]*\\($\\|[ \t]\\)[ \t\n]*"
            sentence-end-double-space nil)

      ;; I typically want to use UTF-8.
      (prefer-coding-system 'utf-8)

      ;; Nicer handling of regions.
      (transient-mark-mode 1)

      ;; Make moving cursor past bottom only scroll a single line rather
      ;; than half a page.
      (setq scroll-step 1
            scroll-conservatively 5)

      ;; Enable highlighting of current line.
      (global-hl-line-mode 1)

      ;; Improved handling of clipboard in GNU/Linux and otherwise.
      (setq select-enable-clipboard t
            select-enable-primary t
            save-interprogram-paste-before-kill t)

      ;; Pasting with middle click should insert at point, not where the
      ;; click happened.
      (setq mouse-yank-at-point t)

      ;; Enable a few useful commands that are initially disabled.
      (put 'upcase-region 'disabled nil)
      (put 'downcase-region 'disabled nil)

      (setq custom-file (locate-user-emacs-file "custom.el"))
      (load custom-file)

      ;; When finding file in non-existing directory, offer to create the
      ;; parent directory.
      (defun with-buffer-name-prompt-and-make-subdirs ()
        (let ((parent-directory (file-name-directory buffer-file-name)))
          (when (and (not (file-exists-p parent-directory))
                     (y-or-n-p (format "Directory `%s' does not exist! Create it? " parent-directory)))
            (make-directory parent-directory t))))

      (add-to-list 'find-file-not-found-functions #'with-buffer-name-prompt-and-make-subdirs)

      ;; Don't want to complete .hi files.
      (add-to-list 'completion-ignored-extensions ".hi")

      (defun rah-disable-trailing-whitespace-mode ()
        (setq show-trailing-whitespace nil))

      ;; Shouldn't highlight trailing spaces in terminal mode.
      (add-hook 'term-mode #'rah-disable-trailing-whitespace-mode)
      (add-hook 'term-mode-hook #'rah-disable-trailing-whitespace-mode)

      (defun rah-prog-mode-setup ()
        ;; Use a bit wider fill column width in programming modes
        ;; since we often work with indentation to start with.
        (setq fill-column 80))

      (add-hook 'prog-mode-hook #'rah-prog-mode-setup)

      (defun rah-sort-lines-ignore-case ()
        (interactive)
        (let ((sort-fold-case t))
          (call-interactively 'sort-lines)))
         '';
      };
  };

  home.packages = [ doom-emacs ];
  home.file.".emacs.d/init.el".text = ''
      (load "default.el")
  '';

*/
  programs.fish = {
                    enable = true;
                      plugins = [     # oh-my-fish plugins are stored in their own repositories, which
          # makes them simple to import into home-manager.
          {
            name = "fasd";
            src = pkgs.fetchFromGitHub {
              owner = "oh-my-fish";
              repo = "plugin-fasd";
              rev = "38a5b6b6011106092009549e52249c6d6f501fba";
              sha256 = "06v37hqy5yrv5a6ssd1p3cjd9y3hnp19d3ab7dag56fs1qmgyhbs";
            };
          }];
		    };
  # import /etc/nix/user_emacs.nix {inherit sources config pkgs; }
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "vamshi";
  home.homeDirectory = "/home/vamshi";
  #xdg.dataHome = "${home.homeDirectory}/.local/share";
  #xdg.configHome = "${home.homeDirectory}/.config";
  #xdg.userDirs = {
  #    enable = true;
  #    desktop = "${home.homeDirectory}/desktop";
  #    documents = "${home.homeDirectory}/docs";
  #    download = "${home.homeDirectory}/dl";
  #    music = "${home.homeDirectory}/music";
  #    pictures = "${home.homeDirectory}/Pictures";
  #    publicShare = "${home.homeDirectory}/public";
  #    templates = "${home.homeDirectory}/templates";
  #  };
  services.picom.enable = true;
  #services.random-background = {
  #    enable = true;
  #    imageDirectory = "/home/vamshi/Pictures/wallpapers";
  #  };
  programs.git = {
    enable = true;
    userName  = "vamshi krishna";
    userEmail = "benb6768@gmail.com";
  }; 
  #xmonad
  #xsession.windowManager.xmonad.enable
 xsession.windowManager.xmonad = {
                                    enable = true;
				    enableContribAndExtras = true;
				    config = /etc/nixos/dotfiles/xmonad/xmonad.hs;
				    extraPackages =  haskellPackages: [
                                                                            haskellPackages.xmonad-contrib
                                                        #./dotfiles/lib/Custom.MyProject
                                                                            haskellPackages.xmonad-wallpaper
                                                                            #haskellPackages.random
                                                                            haskellPackages.xmonad-contrib
                                                                            haskellPackages.xmonad-extras
                                                                            #haskellPackages.xmonad
                                                                            haskellPackages.X11
									    #haskellPackages.taffybar
                                                                            #haskellPackages.filepath_1_4_2_1
						       ];
				};
 
  #redshift
  services.redshift = {
                       enable = true;
		       latitude = "17.492829";
		       longitude = "78.281284";
		       extraOptions = ["-g" "0.7:0.7:0.7" ];
		       temperature = {
		                       day = 3400;
				       night = 3400;
				    };
		       brightness  = {
                                      day = "0.65";
				       night = "0.45";
                       };
  }; 
  programs.i3status.enable = true;
  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.03";
}
