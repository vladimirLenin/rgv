# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, sources, ... }:

with pkgs;
let
  my-python-packages = python-packages: with python-packages; [
    #batmon
    #pandas
    #requests
    # other python packages you want
  ]; 
  python-with-my-packages = python3.withPackages my-python-packages;

    all-hies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};
  notifications-tray-icon-source = pkgs.fetchFromGitHub {
    owner = "IvanMalison";
    repo = "notifications-tray-icon";
    rev = "a855ebf924af3d695c5a10caca34b4eb88f58afb";
    sha256 = "1pd7jhapz080v9q9iv7g8jk9an24zkipmfgg9fmfjr1qjv1zdbib";
  };
  notifications-tray-icon = (import (notifications-tray-icon-source.outPath + "/default.nix"));


in 
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      (import "${builtins.fetchTarball https://github.com/rycee/home-manager/archive/master.tar.gz}/nixos")
    ];

  nixpkgs.config.packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
      inherit pkgs;
    };
  };

  # taffybar
  nixpkgs.overlays = [
    (import ./overlays.nix)
    (import ./dotfiles/taffybar/taffybar/environment.nix)
  ];

 nixpkgs.config.permittedInsecurePackages = [
    "openssl-1.0.2u"
  ];

/*
builtins.fetchTarball {
  # Get the revision by choosing a version from https://github.com/nix-community/NUR/commits/master
  url = "https://github.com/nix-community/NUR/archive/3a6a6f4da737da41e27922ce2cfacf68a109ebce.tar.gz";
  # Get the hash by running `nix-prefetch-url --unpack <url>` on the above url
  sha256 = "04387gzgl8y555b3lkz9aiw9xsldfg4zmzp930m62qw8zbrvrshd";
}*/

  home-manager.users.vamshi = import ./dotfiles/home-manager/home.nix { inherit pkgs config lib sources; };
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.plymouth.enable = true;

  # This includes support for suspend-to-RAM and power-save features on laptops.
  powerManagement = {
    enable = true;
  };

  networking.hostName = "cosmos"; # Define your hostname.
  networking.wireless = {
                       enable = true;  # Enables wireless support via wpa_supplicant.
	     networks = {
                           # HOME
                           "Varikuti" = {
                                            pskRaw = "2f5385967f945b489e113af64fb060b86319cf195898249a8cd0189e63078cbd"; # "9be2248888cc9c79b7f81aef7a17c9f3f6be1e33e19a573b5c0a8178831307c6";
      };
     };
};



  # Set your time zone.
  time.timeZone = "Asia/Kolkata";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp1s0.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_IN.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };
  xdg.menus.enable = true;

 /* # BitTorrent client:
  services.transmission = {
    enable = true;
    user = "transmission";
    group = "transmission";
    port = 9091;
    #settings = {
      #download-dir = "/var/lib/transmission/Downloads";
      #incomplete-dir = "/var/lib/transmission/.incomplete";
      #incomplete-dir-enabled = true;
    #};
  }; */

  # Enable the Plasma 5 Desktop Environment.
  services.xserver.enable = true;
  #services.xserver.displayManager.sddm.enable = true;
 # services.xserver.windowManager.xmonad.enable = true;
 # Enable xmonad and include extra packages
 services.xserver = 
 {
     
     # Configure keymap in X11
     layout = "us";
     xkbOptions = "eurosign:e";
     libinput= {
                 enable = true;
                 naturalScrolling = true;      # Enable touchpad support (enabled default in most desktopManager).

		  additionalOptions = ''
                                       Option "AccelSpeed" "1.0"        # Mouse sensivity
                                       Option "TapButton2" "0"          # Disable two finger tap
                                       Option "VertScrollDelta" "-180"  # scroll sensitivity
                                       Option "HorizScrollDelta" "-180"
                                       Option "FingerLow" "40"          # when finger pressure drops below this value, the driver counts it as a release.
                                       Option "FingerHigh" "70"
                                    '';
		     };
     displayManager = {
                        defaultSession = "none+xmonad";
			#lightdm.autologin = {enable = true; user = "vamshi";};
                        lightdm.greeters.mini = {
                        enable = true;
                        user = "vamshi";
                        extraConfig = ''
                                [greeter]
                                show-password-label = false
                                [greeter-theme]
                                background-image = ""
                                '';
                        };
                };
     windowManager.xmonad.enable = true;
  };
#  # Enable cron service
#  services.cron = {
#    enable = true;
#    systemCronJobs = [
#      "*/5 * * * *     /home/vamshi/.xmonad/restart.sh"
#    ];
#  }; */
 #   location.latitude  = 17.492829;
 #   location.longitude = 78.281284;
  # Services
  # Redshift
 # services.redshift = {
 #   enable = true;
 #   brightness = {
      # Note the string values below.
 #     day = "0.67";
 #     night = "0.54";
 #   };
 #   temperature = {
 #     day = 3400;
 #     night = 3400;
 #   };
 # };
    # bluetooth
   hardware.bluetooth = {
       enable = true;
       powerOnBoot = true;
           extraConfig = ''
                 [General]
                Enable=Source,Sink,Media,Socket
                '';
                };
 # services.blueman.enable = true;
  sound.enable = true;
  hardware.pulseaudio = {
               enable = true;
               extraModules = [ pkgs.pulseaudio-modules-bt ];
               package = pkgs.pulseaudioFull;
               support32Bit = true; # Steam
               extraConfig = ''
               load-module module-bluetooth-policy auto_switch=2
	       load-module module-switch-on-connect
	       load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1
                 '';
};
# Music daemon, can be accessed through mpc or an other client
   services.mpd = {
         enable = true;
         extraConfig = ''
         audio_output {
         type "pulse" # MPD must use Pulseaudio
         name "Pulseaudio" # Whatever you want
         server "127.0.0.1" # MPD must connect to the local sound server
           }
       '';
    };

   # light
  programs.light.enable = true;
  services.actkbd = {
    enable = true;
    bindings = [
      { keys = [ 224 ]; events = [ "key" ]; command = "/run/current-system/sw/bin/light -A 10"; }
      { keys = [ 225 ]; events = [ "key" ]; command = "/run/current-system/sw/bin/light -U 10"; }
    ];
  };
  #services.xserver.
 # services.xserver.  
  # Enable UPower, which is used by taffybar.
  services.upower.enable = true;
  systemd.services.upower.enable = true;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  #sound.enable = true;
  #hardware.pulseaudio.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vamshi = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  };

    # Security and networking
  security.sudo.wheelNeedsPassword = false;
 
  # Fonts
/*  fonts = {
    enableFontDir = true;
    enableDefaultFonts = true;
  fonts = with pkgs; [
      dejavu_fonts
      emojione
      fira-code
      fira-code-symbols
      powerline-fonts
      font-awesome-ttf
      noto-fonts-emoji
      roboto
      #sans
      #lucida-MAC
      #lucida-grande
      source-code-pro
      source-sans-pro
      source-serif-pro
      twemoji-color-font
    ];
     fontconfig = {
      defaultFonts = {
        monospace = [ "Fira Code Mono" ];
        sansSerif = [ "Roboto" ];
        serif     = [ "Source Serif Pro" ];
      };
    }; 
  }; */
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.android_sdk.accept_license = true;

  services.emacs.enable = true;
  #services.emacs.package = import /etc/nixos/dotfiles/emacs.d { pkgs = pkgs; };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    
    # Emacs
 #   (import /etc/nix/user_emacs.nix {inherit sources config pkgs; })
 #   (import /etc/nixos/dotfiles/emacs.d/emacs.nix { inherit pkgs; })
 #   Tools
    wget  feh unzip xz automake gnumake notify-desktop libnotify  notify-osd-customizable  inotify-tools scrot  binutils-unwrapped zlib 
    # Battery
    acpi #fontconfig
    # Nix
    nix-prefetch-git
    cachix
    # Applications
    firefox brave pcmanfm okular dmenu pulsemixer redshift transmission-qt libreoffice lxappearance lxappearance-gtk2
    # Appearance
    gnome-breeze gnome3.adwaita-icon-theme hicolor-icon-theme materia-theme numix-icon-theme-circle papirus-icon-theme plasma5.breeze-gtk  plasma5.breeze-qt5 material-design-icons
    # xorg
    wmctrl xclip xdotool xorg.xev xorg.xdpyinfo xorg.xkbcomp xorg.xwininfo xsettingsd xorg.xbacklight
    # terminal
    konsole alacritty terminator rxvt-unicode xterm htop  neovim vim cool-retro-term tmux
    # lang
    # scheme
    #scheme48
    # racket
    racket
    # haskell
    # haskellPackages.status-notifier-item
    cabal-install ghc cabal2nix haskellPackages.status-notifier-item  #haskellPackages.gtk-sni-tray
    #haskellPackages.hpack
    #haskellPackages.hasktags
  #  haskellPackages.gi-cairo-connector
    haskellPackages.hoogle
    #xmonad-with-packages
    #haskellPackages.dbus-hslogger
    # dhall
    #haskellPackages.dhall
    #haskellPackages.dhall-json
    #stack
    #stack2nix
    (import ./dotfiles/taffybar/default.nix)
 #   (import ./dotfiles/config/xmonad/default.nix)
 #  (import ./dotfiles/config/gtk-sni-tray/default.nix)
 #   (import ./dotfiles/config/status-notifier-item/default.nix)
    # (import ./dotfiles/gtk-sni-tray/default.nix)
    #   haskellPackages.dbus-hslogger
    # taffybar
    #    (import ./dotfiles/config/taffybar/default.nix)
    # python
    python-with-my-packages  python.pkgs.pip
    # git
    git yadm
    # media players 
    spotify vlc cmus
    # Editing
    libsForQt514.kdenlive    kdeApplications.kdenlive obs-studio screenkey audacity
    # Audio Control
    pavucontrol
   # (import /home/vamshi/.emacs.d/emacs.nix { inherit pkgs; })
  ];

 # nixpkgs.config.allowBroken = true;
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

}

